/**
 *  Los ciclos del jardín cosmopolita
 *  ale - @414c45
 */

PGraphics canvas;

int canvas_background = #111111;
int
  canvas_x,
  canvas_y,
  canvas_center_x,
  canvas_center_y,
  canvas_width,
  canvas_height;
Month[] months;

void setup()
{
    // Set graphical settings
    size(displayWidth, displayHeight);
    background(#222222);

    // Set initial data
    months = new Month[12];
    Table data = loadTable(
      "data--calendar.csv",
      "header"
    );
    int i = 0;
    for(TableRow row : data.rows()){
        months[i] = new Month(
            i,
            row.getInt("flower_cal_families"),
            row.getInt("flower_cal_individuals"),
            row.getInt("fruit_cale_families"),
            row.getInt("fruit_cale_individuals"),
            row.getInt("aroma_cale_families"),
            row.getInt("aroma_cale_individuals")
        );
        i++;
    }

    // Canvas set-up
    canvas_width  = height;
    canvas_height = height;
    canvas = createGraphics(canvas_width, canvas_height);
    canvas.beginDraw();
    canvas.background(canvas_background);
    canvas.endDraw();
    canvas_x = width - height;
    canvas_y = 0;
    canvas_center_x = canvas_x + canvas_width/2;
    canvas_center_y = canvas_y + canvas_height/2;
}

void draw(){
    image(canvas, canvas_x, canvas_y);
}
