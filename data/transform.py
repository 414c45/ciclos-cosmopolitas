#!/usr/bin/python3

import csv

#
# Refine
# Transforms plants data into a 'calendar' representation
#

input_fieldnames = [
    'flower_cal',
    'fruit_cale',
    'aroma_cale',
]

output_fieldnames = [
    'flower_cal_families',
    'flower_cal_individuals',
    'fruit_cale_families',
    'fruit_cale_individuals',
    'aroma_cale_families',
    'aroma_cale_individuals',
]

with open('data/data--grouped.csv', 'r') as input:
    with open('data/data--calendar.csv', 'w') as output:
        reader = csv.DictReader(input, delimiter=',')
        writer = csv.DictWriter(output, fieldnames=output_fieldnames)
        writer.writeheader()
        output = { "%s"%month: { field : 0 for field in output_fieldnames } for month in range(1,13) }
        for specie in reader:
            for field in input_fieldnames:
                field_calendar = specie[field]
                for month, value in enumerate(field_calendar, 1):
                    if value != '0':
                        output["%s" % month]['%s_families' % field] += 1
                        output["%s" % month]['%s_individuals' % field] += int(specie['count']) * int(value)
        for month in range(1,13):
            writer.writerow(output["%s"%month])
