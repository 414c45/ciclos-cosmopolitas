#!/usr/bin/python3

#
# Refine
# Clean data and group it by species
#

import csv

fieldnames = [
    'common_nam',
    'count',
    'scientific',
    'family',
    'origin',
    'height_max',
    'height_min',
    'radius',
    'flower_col',
    'type',
    'foliage_ty',
    'flower_app',
    'flower_cal',
    'fruit_cale',
    'aroma_cale',
    'sunstroke',
    'water_cons',
    'c02_range',
    'co2_absorb',
]

with open('data.csv', 'r') as input:
    with open('data--grouped', 'w') as output:
        reader = csv.DictReader(input, delimiter=',')
        writer = csv.DictWriter(output, fieldnames=fieldnames)
        writer.writeheader()
        current_name = ''
        count        = 0
        last_row     = { field: '' for field in fieldnames }
        num_fields   = ['height_max', 'height_min', 'radius', 'co2_absorb']
        list_fields  = ['flower_cal', 'fruit_cale', 'aroma_cale']
        for row in reader:
            common_name = row['common_nam']
            if common_name != current_name:
                out_row = {}
                for field in fieldnames:
                    value = last_row[field] if field != 'count' else count
                    if field in num_fields and (field not in list_fields) and value:
                        value = "{:1.00f}".format( float(value) )
                    out_row[field] = value
                if out_row['count'] != 0:
                    writer.writerow(out_row)
                current_name = common_name
                count = 0
            else:
                count += 1
                last_row = row
