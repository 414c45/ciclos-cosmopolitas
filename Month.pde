/**
 *  Months
 *  Month names in an awkward Java-style
 */

static class Months
{
    public static String[] names = {
          "Enero",
          "Febrero",
          "Marzo",
          "Abril",
          "Mayo",
          "Junio",
          "Julio",
          "Agosto",
          "Septiembre",
          "Octubre",
          "Noviembre",
          "Diciembre"
    };
}

/**
 *  Month
 *  A class to handle and manipulate month-related data
 */
class Month
{
    int
      flower_families_number,
      flower_individuals_number,
      fruit_families_number,
      fruit_individuals_number,
      aroma_families_number,
      aroma_individuals_number,
      total_families,
      total_individuals;

    String name;

    // Constructor
    Month(int month_number, int fl_fam, int fl_ind, int fr_fam, int fr_ind, int ar_fam, int ar_ind){
        this.name = Months.names[month_number];
        this.flower_families_number = fl_fam;
        this.flower_individuals_number = fl_ind;
        this.fruit_families_number = fr_fam;
        this.fruit_individuals_number = fr_ind;
        this.aroma_families_number = ar_fam;
        this.aroma_individuals_number = ar_ind;
        this.total_families    = fl_fam + fr_fam + ar_fam;
        this.total_individuals = fl_ind + fr_ind + ar_ind;
    }

    // toString
    String toString() {
      return
          "Name: " +
          this.name +
          " · Total families: " +
          this.total_families +
          " · Total individuals: " +
          this.total_individuals;
    }
}
